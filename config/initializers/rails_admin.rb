RailsAdmin.config do |config|

	### Popular gems integration

	## == Devise ==
	config.authenticate_with do
		warden.authenticate! scope: :user
	end
	config.current_user_method(&:current_user)

	## == Cancan ==
	config.authorize_with :cancan

	config.model StaticPage do
		edit do
			# For RailsAdmin >= 0.5.0
			field :page_name, :string
			field :page_title, :string
			field :is_active, :boolean
			field :page_content, :ck_editor
			# For RailsAdmin < 0.5.0
			# field :description do
			#   ckeditor true
			# end
		end
	end
	config.model Promocode do
		edit do
			# For RailsAdmin >= 0.5.0
			field :code, :string
			field :validity, :string
			field :is_active, :boolean
		end
	end
	## == PaperTrail ==
	# config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

	### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

	config.actions do
		dashboard                     # mandatory
		index                         # mandatory
		new
		export
		bulk_delete
		show
		edit
		delete
		show_in_app

		## With an audit adapter, you can add:
		# history_index
		# history_show
	end
end
