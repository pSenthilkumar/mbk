class OnlyAjaxRequest
  def matches?(request)
    request.xhr?
  end
end

Rails.application.routes.draw do
  resources :promocodes

  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, :controllers => {:registrations => "users/registrations",:omniauth_callbacks => "users/omniauth_callbacks"}
  
  root 'home#index'

  get "events/delete_all", :constraints => OnlyAjaxRequest.new
  get "events/unparticipate", :constraints => OnlyAjaxRequest.new
  
  get "events/participate/:id"=>"events#participate", :constraints => OnlyAjaxRequest.new

  # get "messages/delete_all", :constraints => OnlyAjaxRequest.new
  # get "messages/restore", :constraints => OnlyAjaxRequest.new
  # post "messages/reply/:id"=>"messages#reply", :constraints => OnlyAjaxRequest.new
  get 'users/users_for_select', :constraints => OnlyAjaxRequest.new
  get 'profile/report_abuse', :constraints => OnlyAjaxRequest.new
  post 'subscribePromo'=>"users#subscribe_promocode",:constraints => OnlyAjaxRequest.new
  post 'users/requestFriend', :constraints => OnlyAjaxRequest.new
  post 'users/acceptFriend', :constraints => OnlyAjaxRequest.new
  post 'users/subscribe', :constraints => OnlyAjaxRequest.new
  post 'users/unsubscribe', :constraints => OnlyAjaxRequest.new
  get 'profile/viewed_me'
  post 'messages/contactUs', :constraints => OnlyAjaxRequest.new
  get 'users/invitations', :constraints => OnlyAjaxRequest.new
  post 'users/updatePassword', :constraints => OnlyAjaxRequest.new
  # post 'messages/sendMessage', :constraints => OnlyAjaxRequest.new

  resources :events, :constraints => OnlyAjaxRequest.new
  resources :saved_searches, :constraints => OnlyAjaxRequest.new

  resources :messages, :constraints => OnlyAjaxRequest.new
  resources :reply, :constraints => OnlyAjaxRequest.new
  resources :profile, :constraints => OnlyAjaxRequest.new
  resources :image, :constraints => OnlyAjaxRequest.new
  resources :search, :constraints => OnlyAjaxRequest.new
  resources :stripe, :constraints => OnlyAjaxRequest.new
  resources :static_pages, :constraints => OnlyAjaxRequest.new
  get "makeFavourite" =>  "profile#makeFavourite"

  get '*path', to: "home#index", format: :html

end
