require "rvm/capistrano"
require "bundler/capistrano"


server "162.243.207.144", :app, :web, :db, :primary => true
set :deploy_to, "/var/www/mustbekarma/"
set :branch, 'master'
set :scm_verbose, true
set :use_sudo, false
set :rails_env, "development" #added for delayed job 
set :rvm_type, :system


after 'deploy:update_code' do
  run "cd #{release_path}"
  
  # run "rm -rf #{release_path}/tmp"
  run "chmod -R 777 #{release_path}/tmp/"
  # run "ln -s #{shared_path}/system/tmp #{release_path}/"


  run "rm -rf #{release_path}/public/system"
  run "ln -s #{shared_path}/system/ #{release_path}/public/" 

  run "rm -rf #{release_path}/public/uploads"
  run "ln -s #{shared_path}/uploads  #{release_path}/public/"

  run "rm -rf #{release_path}/public/s3"
  run "ln -s #{shared_path}/uploads/s3  #{release_path}/public/"

  run "cd #{release_path} && bundle install"
end

namespace :deploy do
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(shared_path,'tmp','restart.txt')}"
  end
end