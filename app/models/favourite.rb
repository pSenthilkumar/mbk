class Favourite
  include Mongoid::Document

  belongs_to :user

  field :user_id, type: String
  field :favorite_user_id, type: String
end
