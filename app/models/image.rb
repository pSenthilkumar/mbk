class Image
  include Mongoid::Document
  field :file, type: String
  mount_uploader :file, ImageUploader

  field :profile_pic, type: Boolean
  
  belongs_to :user
end
