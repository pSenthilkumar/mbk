class Abuse
  include Mongoid::Document
  belongs_to :user, :inverse_of=>:abuses
  belongs_to :abused_by_me, :class_name=>"User",:foreign_key=>"abuser_id", :inverse_of=>:abused_by_others
end
