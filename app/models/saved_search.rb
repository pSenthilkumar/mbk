class SavedSearch
  include Mongoid::Document
  include Mongoid::Timestamps

  field :search_name, :type=>String
  field :filters, :type=>Object
  belongs_to :user, :inverse_of => :saved_searches
end
