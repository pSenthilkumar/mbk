class UserView
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :user, :inverse_of=>:viewed_profiles
  belongs_to :viewed, :foreign_key=>"viewed_id", :class_name=>"User", :inverse_of=>:viewed_me

end
