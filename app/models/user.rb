class User
	include Mongoid::Document
	include Mongoid::Search
	include Mongoid::Timestamps

	devise :database_authenticatable, :registerable,
	:recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

	## Database authenticatable
	field :email,              type: String, default: ""
	field :encrypted_password, type: String, default: ""

	## Recoverable
	field :reset_password_token,   type: String
	field :reset_password_sent_at, type: Time

	## Rememberable
	field :remember_created_at, type: Time

	## Trackable
	field :sign_in_count,      type: Integer, default: 0
	field :current_sign_in_at, type: Time
	field :last_sign_in_at,    type: Time
	field :current_sign_in_ip, type: String
	field :last_sign_in_ip,    type: String
	field :last_seen,          type: Time
	field :customerStripToken, type: String
	field :is_profile_filled,  type: Boolean
	field :is_admin,           type: Boolean, default: false
	field :provider, 					 type: String
	field :uid,								 type: String
	# Relationships 
	has_many :events, :dependent=>:detroy, inverse_of: :user

	has_many :saved_searches
	has_and_belongs_to_many :participated_events, :class_name=>"Event", :inverse_of=> :participants

	has_many :friends, :class_name=>"User"
	belongs_to :friend, :class_name=>"User", :inverse_of=>:friends

	has_many :acceptances, :class_name=>"Invitation",:foreign_key=>"friend_id"
	has_many :invitations

	has_many :user_views
	has_many :viewed_me, :class_name=>"UserView", :foreign_key=>"viewed_id"
	# association for messages
	has_many :sent_messages, :class_name=>'Message', :foreign_key=>'sender_id'
	has_many :received_messages, :class_name=>'Message',
	:foreign_key=>'receiver_id'
	has_many :abuses, :inverse_of=>:user
	has_many :other_abuses, :class_name=>"Abuse",:foreign_key=>"abuser_id"
	# association for Profile

	embeds_one :profile
	embeds_one :payment
	has_many :images

	has_many :favourites

	
	def markFavourite(users)
		filteredUser = []
		favIds = self.favourites.pluck(:favorite_user_id)
		friends = self.friends
		users.each do |user|
			if favIds.include? user.id.to_s
				user["favourite"] = true
			else
				user["favourite"] = false
			end
			if friends.include? user
				user["isFriend"] = true
			else
				user["isFriend"] = false
			end
			user["images"] = user.images
			user["profileImage"] = user.images.where(:profile_pic => true).first
			filteredUser << user
		end
		return filteredUser
	end
	def invite_user(userId)
		return self.invitations.create(:friend_id=>userId,:status=>"Invited").persisted?
	end
	def accept_user(user)
		check_record = self.acceptances.where(:user_id=>user.id)
		if check_record.length > 0
			check_record.first.delete()
			self.friends.push(user)
			user.friends.push(self)
		end
	end

	def pickThread(user)
		Message.any_of({:sender_id => user["user_id"],:receiver_id => self.id},{:receiver_id => user["user_id"],:sender_id => self.id})
	end

	def self.sendMail(user_id,mailType,options={})
		@user = User.find(user_id)
		unless @user.nil?
			case mailType
			when "welcome"
				UserMailer.welcome_email(@user).deliver
			when "subscription"
				UserMailer.subscription(@user).deliver
			when "subscriptionCancel"
				UserMailer.subscription_cancel(@user).deliver
			when "promotionMail"
				UserMailer.promoted_mail(@user).deliver
			when "updatePassword"
				UserMailer.update_password(@user).deliver
			when "consolidated"
				UserMailer.consolidated_mail(@user,options).deliver
			end
		end
	end
	def self.from_omniauth(auth)
		any_of({:provider=> auth.provider, :uid=> auth.uid},{:email=>auth.info.email}).first_or_create do |user|
			user.email = auth.info.email
			user.password = Devise.friendly_token[0,20]
			user.build_profile unless user.persisted?	
			user.profile.name = auth.info.name
			user.provider = auth.provider
			user.uid= auth.uid
			user.save()
		end
	end

	def self.ConsolidatedMail
		users = self.where(:is_admin=>false)
		users.each do | user|
			options = {}
			options["message_count"] = user.received_messages.between(:created_at=>Date.today..Date.today+1).count
			options["event_count"] =  Event.between(:created_at=>Date.today..Date.today+1).count
			options["profileViews"] = user.viewed_me.between(:created_at=>Date.today..Date.today+1).count
			self.delay.sendMail(user.id,"consolidated",options)
		end

	end
end
