class Event
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Search
  include Mongoid::Timestamps
  field :event_name, type: String
  field :address, type: String
  field :city, type: String
  field :state, type: String
  field :zip_code, type: String
  field :event_date, type: Date
  field :time_from, type: DateTime
  field :time_to, type: DateTime
  field :message, type: String
  belongs_to :user, inverse_of: :event
  has_and_belongs_to_many :participants, :class_name=>"User",:inverse_of=>:participated_events

  search_in :event_name, :address, :city, :message
  
end
