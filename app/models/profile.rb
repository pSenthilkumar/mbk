class Profile
  include Mongoid::Document

  before_save :update_mandatory_fill
  embedded_in :user
  belongs_to :promocode
  @@require_attrs =  [:tag_line,:name,:gender,:age,:birth_month,:birth_day,:location,:height,:body_type,:ethnicity,:zodiac,:religion,:political_views,:looking_for,
    :min_age,:max_age,:relationship_type,:other_qualities]

  # About me
  field :tag_line , type: String
  field :name,  type: String
  field :gender,  type: String
  field :age, type: Integer
  field :birth_month,  type: String
  field :birth_day, type: Integer
  # field :country,  type: String
  # field :state,  type: String
  # field :city,  type: String
  # field :zip,  type: String
  field :location, type: String
  field :height,  type: String
  field :body_type,  type: String
  field :ethnicity,  type: String
  field :zodiac,  type: String
  field :religion,  type: String
  field :political_views,  type: String
  field :tags,  type: Array

  # Looking_for
  field :looking_for,  type: String
  field :min_age, type: Integer
  field :max_age, type: Integer
  field :relationship_type,  type: String
  field :other_qualities , type: String

  # Relationship
  field :relationship_status,  type: String
  field :long_relationship,  type: String
  field :eye_color,  type: String
  field :hair_color,  type: String
  field :income, type: Integer
  field :income_type,  type: String
  field :profession,  type: String


  # Education & Qualities
  field :education,  type: String
  field :smoker, type: Boolean
  field :go_with_smoker, type: Boolean
  field :drug_adict, type: Boolean
  field :go_with_drug_addict, type: Boolean
  field :have_children, type: Boolean
  field :go_with_have_children, type: Boolean
  field :need_children, type: Boolean
  field :have_car, type: Boolean
  field :drinker, type: Boolean
  field :go_with_drinker, type: Boolean
  field :have_pets, type: Boolean
  field :pet_name,  type: String
  field :go_with_has_pets, type: Boolean
  field :believe_karma, type: Boolean
  field :go_with_not_believe_karma, type: Boolean



  # Description
  field :zen_moment , type: String
  field :get_inspired , type: String
  field :good_karma_points , type: String
  field :bad_karma_points , type: String
  field :my_type_if , type: String
  field :others_interested_about , type: String

  field :latitude , type: Float
  field :longitude, type: Float
  # field :user_id, type: String
  field :privacy_status, type: Boolean

  field :created_at, type: Time
  field :updated_at, type: Time 
  field :is_public, type: Boolean, default: false
  field :paymentStatus, type: String
  field :is_profile_filled, type: Boolean, default: false
  field :promocode_id, type: String
  def update_mandatory_fill
    filled = true
    @@require_attrs.each do |attr|
      if self.send(attr).blank? 
        filled = false
        break
      end
    end
    self.is_profile_filled=filled
    true
  end
end
