class Invitation
  include Mongoid::Document
  include Mongoid::Timestamps

  field :status

  belongs_to :accepted_friend, :class_name=>"User",:foreign_key=>"friend_id",:inverse_of=>:acceptances
  belongs_to :user, :inverse_of=>:invitations

end
