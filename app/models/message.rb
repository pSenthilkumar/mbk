
class Message
  include Mongoid::Document
  include Mongoid::Search
  include Mongoid::Timestamps

  belongs_to :sender, :class_name=>'User', :foreign_key=>'sender_id',inverse_of: :sent_messages
  belongs_to :receiver, :class_name=>'User', :foreign_key=>'receiver_id',inverse_of: :received_messages
  embeds_many :replies

  field :sender_id, type: String
  field :receiver_id, type: String
  field :message, type: String
  field :sender_status, type: String
  field :receiver_status, type: String
  field :sender_trash, type: Time
  field :receiver_trash, type: Time

  # scope :deleted, lambda { |user|
  #   any_of({:sender_id => user.id,:sender_status=>'deleted'},{:receiver_id=>user.id,:receiver_status=>'deleted'})
  # }

  # scope :read, ->{ where(:receiver_status=>"read") }
  # scope :unread, -> {where(:receiver_status=>"unread")}
  # scope :inbox, -> { any_of({:receiver_status=>'read', :receiver_status=>'unread'}) } 
  # scope :sent, -> { where(:sender_status=>"sent") }
  # search_in :subject, :message, :sender_status,:receiver_status
  # scope :active, lambda { |message|
  #   { where(:id => message.id) }
  # }

  def activeReplies(user)
    trashTime = (self.receiver_id == user.id)? self.receiver_trash : self.sender_trash
    whoIAm = (self.receiver_id == user.id.to_s)? 'receiver' : 'sender'

    if(whoIAm == 'receiver')
      friendId = self.sender_id
    else
      friendId = self.receiver_id
    end
    trashTime = (Date.today - 365.days) if trashTime.nil?
    replies = self.replies.gte(created_at: trashTime).pluck(:created_at,:reply_message,:creator_id)
    # replyByMyFriend= self.replies.gte(created_at: trashTime).where(:creator_id => friendId).pluck(:created_at,:reply_message,:creator_id)
    # replies = replyByMe + replyByMyFriend    
    # binding.pry
    replies
  end
end
