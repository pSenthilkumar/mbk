class Reply
  include Mongoid::Document
  include Mongoid::Timestamps
  attr_accessor :creator
  field :reply_message, type: String
  field :creator_id, type: String
  field :created_at, type: Time
  embedded_in :message, :inverse_of => :replies
  belongs_to :creator, :class_name=>"User", :inverse_of=> :user_replies
  
  # def creator
  # 	user = User.find(self.creator_id)
  # 	return { "email"=>user.email,"id"=>user.id}
 	# end
end
