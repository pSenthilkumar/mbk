class StaticPage
  include Mongoid::Document
  field :page_name, type: String
  field :page_content, type: String
  field :is_active, type: Mongoid::Boolean
  field :page_title, type: String
  validates :page_name, uniqueness: true,presence: true
end
