class Payment
  include Mongoid::Document
  field :response, type: String
  field :stripId, type: String
  field :subcriptionId, type: String
  embedded_in :user
end
