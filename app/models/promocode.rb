class Promocode
  include Mongoid::Document
	include Mongoid::Timestamps
  field :validity, type: Integer
  field :is_active, type: Mongoid::Boolean
  field :code, type: String
  validates :code, uniqueness: true,presence: true
end
