class PromocodesController < ApplicationController
  before_action :set_promocode, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @promocodes = Promocode.all
    respond_with(@promocodes)
  end

  def show
    respond_with(@promocode)
  end

  def new
    @promocode = Promocode.new
    respond_with(@promocode)
  end

  def edit
  end

  def create
    @promocode = Promocode.new(promocode_params)
    @promocode.save
    respond_with(@promocode)
  end

  def update
    @promocode.update(promocode_params)
    respond_with(@promocode)
  end

  def destroy
    @promocode.destroy
    respond_with(@promocode)
  end

  private
    def set_promocode
      @promocode = Promocode.find(params[:id])
    end

    def promocode_params
      params.require(:promocode).permit(:validity, :active, :code)
    end
end
