class EventsController < ApplicationController
	def create
		if current_user.profile.paymentStatus == 'active'
			render :json => current_user.events.create(event_params)
		else
			render :json =>{:result=>false}, :status => 404
		end

	end

	def index
		puts params
		case params[:type]
		when "my_events"
			@events = current_user.events.includes(:participants)
		when "all_events"
			@events = Event.includes(:participants)
		when "participated"
			@events = current_user.participated_events.includes(:participants)
		when "deleted"
			@events = current_user.events.deleted.includes(:participants)
		end

		@events = @events.search(params[:search]) unless (params[:search]).nil?
		@events = @events.paginate(:page=>params[:page],:per_page=>10)
	end 

	def show
		@event= Event.find(params[:id])
	end

	def participate
		result = false
		@event = Event.find(params[:id])
		unless @event.nil?
			@event.participants.push(current_user)
			message = "New participant in #{@event.event_name}"
			response = {
			          "type" => "notification",
			          "message" => message
			        }

			PrivatePub.publish_to("/#{@event.user_id.to_s}", data: response)
			result = true
		end
		render :json =>{:result=>result} ,status: 201
	end

	def delete_all
		unless params[:ids].nil?
			params[:ids].split(",").each do |id|
				unless params[:delete_type]=="permenant"
					current_user.events.find(id).delete    
				else
					current_user.events.unscoped.find(id).delete!
				end
			end
		end
		render :json =>{:result=>true} ,status: 201
	end

	def destroy
		@event=current_user.events.find(params[:id])
		result = @event.delete ? true : false
		render :json =>{:result=>result} ,status: 201
	end

	def unparticipate
		unless params[:ids].nil?
			events = Event.find(params[:ids].split(","))
			events.each do |evt|
				evt.participants.delete(current_user)
			end
		end
		render :json =>{:result=>true} ,status: 201
	end

	def event_params
		params.require(:event).permit!
	end
end
