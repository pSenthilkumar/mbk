class SearchController < ApplicationController
	def index

		if params[:get] == "recent"
			@users = User.ne(id: current_user.id).includes(:images).desc('_id').limit(params[:limit])
			render :json => current_user.markFavourite(@users)
		end

		if params[:get] == "favourites"
			favIds = current_user.favourites.pluck(:favorite_user_id)
			@users = User.ne(id: current_user.id).includes(:images).find(favIds)
			render :json => current_user.markFavourite(@users)
		end

		if params[:get] == "results"
			filters = {}
			unless params[:filters].nil?
				paramsFilters = JSON.parse(params[:filters]) 
				paramsFilters.each do |key,value|
				  # For Age
					unless key == "min_age" || key == "max_age"
						selectKey = key
					else
						selectKey = "age"
						value = { "$gte" => paramsFilters["min_age"], "$lte" => paramsFilters["max_age"] }
					end

          if key == "tags"
            value = { "$in" => paramsFilters["tags"] }
          end

          
          if key == "name"
            value = /.*#{value}.*/i
          end

					unless value.blank?
						filters["profile.#{selectKey}"] = value
					end
				end
			end
  		users = User.ne(id: current_user.id).where(filters)
  		unless params[:page].nil?
  			params[:limit] ||= 1
  			users = users.paginate(:per_page=>params[:limit],:page=>params[:page])
  			render :json => { "users"=>current_user.markFavourite(users),"total_entries"=>users.total_entries,"total_pages"=>users.total_pages,"current_page"=>users.current_page}
  		else
  			render :json => current_user.markFavourite(users)
  		end
  	end
  end
end