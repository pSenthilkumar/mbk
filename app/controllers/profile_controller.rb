class ProfileController < ApplicationController
  def index
    render :json => current_user.profile
  end

  def create
    if profile_params["_id"].blank?
      render :json => current_user.create_profile(profile_params)
    else
      profile_params.delete("_id")
      profile = current_user.profile
      profile.update_attributes(profile_params)
    end
    render :json=>profile
  end

  def makeFavourite
    favourites = current_user.favourites.where(:favorite_user_id => params[:id])
    if favourites.count > 0
      render :json => favourites.first.delete
    else
      render :json => current_user.favourites.create(:favorite_user_id => params[:id])
    end
  end

  def viewed_me
    user_ids = current_user.viewed_me.pluck(:user_id)
    users = User.find(user_ids)
    render :json=>users
  end

  def show
    user = User.includes(:images).find(params[:id])
    if(user!= current_user)
      current_user.user_views.find_or_create_by(:viewed=>user)
    end
    is_friend = current_user.friends.include?(user)
    @result = {:user=>user.markFavourite([user])[0],:images=>user.images,:isFriend =>is_friend}
  end


  def report_abuse 
    user = User.find(params[:id])
    current_user.abuses.create(:abused_by_me=>user) if current_user.abuses.where(:abuser_id=>user.id).count==0
    render :json=>{:result=>true}
  end
  private
  def profile_params
    params.require(:profile).permit!
  end
end
