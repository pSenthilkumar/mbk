class Users::RegistrationsController < Devise::RegistrationsController
  def create
    super
    if resource.save
      resource.create_profile(:name => params[:user][:name])
      User.delay.sendMail(@user.id,"welcome")
    end
  end
end