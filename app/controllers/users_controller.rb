class UsersController < ApplicationController
	def users_for_select
		receipients =	current_user.friends.map { |user| {"user_id"=>user.id.to_s,"email"=>user.email,"name" => user.profile.name, "last_seen" => user.last_seen}  }
		render :json =>receipients.to_json
	end

	def updatePassword
		user = current_user
		if user.valid_password? params[:data][:currentPassword]
			user.password = params[:data][:newPassword]
			user.password_confirmation = params[:data][:confirmPassword]
			if user.save
				sign_in user, :bypass => true
				User.delay.sendMail(user.id,"updatePassword")
				render :json => true
			else
				render :json => false
			end
		else
			render :json => false
		end
	end

	def requestFriend
		unless current_user.invitations.where(:friend_id => params[:id]).first
			sendInvite = current_user.invite_user(params[:id])
			current_user.sent_messages.create(
				:receiver_id => params[:id],
				:message => "Contact Request",
				:sender_status => "read",
				:receiver_status => "unread")
		end
		render :json => true
	end

	def acceptFriend
		user = User.find(params[:id])
		if current_user.accept_user(user)
			response = {
				"type" =>  "notification",
				"message" => "#{current_user.profile.name} accepted you chat request."
			}
			PrivatePub.publish_to("/#{user.id.to_s}", data: response)
			render :json => true
		else
			render :json => false
		end
	end

	def subscribe
		Stripe.api_key = "sk_test_ranCveoPQ9rMOLhb3erLuF7x"
		customerStrip = Stripe::Customer.create(
			:plan => "monthly", 
			:description => current_user.email,
			:source => params[:token]
			)
		
		payment = current_user.create_payment(:response => customerStrip, :subcriptionId => customerStrip.subscription.id , :stripId => customerStrip.id)
		current_user.profile.paymentStatus = customerStrip.subscriptions.data[0].status
		
		current_user.save
		payment.save
		if customerStrip.subscriptions.data[0].status == 'active'
			User.delay.sendMail(current_user.id,"subscription")
		end
		render :json => {:status => current_user.profile.paymentStatus}
	end

	def unsubscribe
		Stripe.api_key = "sk_test_ranCveoPQ9rMOLhb3erLuF7x"
		customer = Stripe::Customer.retrieve(current_user.payment.stripId)
		customer.subscriptions.retrieve(current_user.payment.subcriptionId).delete
		current_user.profile.paymentStatus = ""
		if current_user.save
			User.delay.sendMail(current_user.id,"subscriptionCancel")
		end
		render :json => true
	end

	def subscribe_promocode
		result = false
		code = Promocode.where(:active=>true,:code=>params[:code]).first
		unless code.nil?
			profile = current_user.profile
			profile.paymentStatus = "active"
			binding.pry
			profile.promocode_id = code.id
			result = true
		end
		render :json=> result
	end
	def invitations
		invitors = []
		current_user.acceptances.includes(:accepted_friend).each do |invite|
			invitor = {} 
			invitor["user_id"] = invite.user_id.to_s
			invitor["email"] = invite.user.email
			invitor["name"] = invite.user.profile.name
			invitor["last_seen"] = invite.user.last_seen
			invitors << invitor
		end
		render :json => invitors
	end
end
