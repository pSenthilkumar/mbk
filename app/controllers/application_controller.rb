class ApplicationController < ActionController::Base
  respond_to :html, :json
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_last_seen_at, if: proc { user_signed_in? }


  private
  def set_last_seen_at
    current_user.update_attribute(:last_seen, Time.now)
  end
end
