class ReplyController < ApplicationController
  def create
    if params[:reply]
      @message = Message.find(params[:reply][:message_id])
      if current_user.profile.paymentStatus == 'active' || @message.replies.count < 4
        unless @message.nil?
          reply = @message.replies.create(:creator_id=>current_user.id,:reply_message=>params[:reply][:reply_message], :created_at => Time.now)
          toSend = []
          toSend << reply.created_at
          toSend << reply.reply_message
          toSend << reply.creator.id
          toSend << reply.creator.profile.name
          response = {
            "type" => "reply",
            "message" => toSend
          }
          PrivatePub.publish_to("/#{params[:reply][:chat_user_id]}", data: response)
          render :json => response, :status=>201
          return
        end
      end
    end
    render :json =>{:result=>false}, :status=>404
  end
end
