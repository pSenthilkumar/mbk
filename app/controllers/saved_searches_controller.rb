class SavedSearchesController < ApplicationController
	def index
		render :json=> current_user.saved_searches
	end
	def create
		render :json => current_user.saved_searches.create(search_params)
	end
	def destroy
		result = false
		search = current_user.saved_searches.find(params[:id])
		result =  search.delete ? true : false unless search.nil?
		render :json=> { "result"=>result}
	end
	private
	def search_params
		params.require(:saved_search).permit!
	end
end
