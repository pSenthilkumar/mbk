class ImageController < ApplicationController
  def index
    render :json => current_user.images.all
  end

  def create
    doc = current_user.images.new()
    doc.file = params[:file]
    if doc.save
      render :json => {:status => true, :id => params[:id], :file => doc}
    else 
      render :json => {:status => false, :id => params[:id], :file => doc}
    end
  end

  def show
    if params[:todo] == 'delete'
      render :json => current_user.images.where(:id => params[:id]).first.delete
    else
      render :json => false
    end
  end

  def update
    current_user.images.update_all(:profile_pic => false)
    render :json => current_user.images.where(:id => params[:id]).first.update_attributes(:profile_pic => image_params["profile_pic"])
  end

  private
  def image_params
    params.require(:image).permit!
  end
end
