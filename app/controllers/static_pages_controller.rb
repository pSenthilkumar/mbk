class StaticPagesController < ApplicationController
	def show
		page_name = params[:id]
		page = StaticPage.find_by(:page_name=>page_name)
		unless page.nil?
			render :json=> page.to_json
		else
			render :json=> false
		end
	end
end
