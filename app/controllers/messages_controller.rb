class MessagesController < ApplicationController
	def index
		# params[:type] ||= "inbox"

		# page = params[:page].nil? ? 1 : params[:page]
		# case params[:type]
		# when "inbox"
		# 	messages = current_user.received_messages.inbox
		# when "sent"
		# 	messages = current_user.sent_messages.sent
		# when "deleted"
		# 	messages = Message.deleted(current_user)
		# end
		# messages = messages.search(params[:search]) unless (params[:search]).blank?
		# @messages = messages.order_by([:created_at,:desc]).paginate(:page=>page,:per_page=>10)
		# message = Message.find(params[:id])
		user = JSON.parse(params[:user])
		message = current_user.pickThread(user).first
		response = {}
		response["replies"] = {
			"sender_name" => message.sender.profile.name,
			"receiver_name" => message.receiver.profile.name,
			"messages" => message.activeReplies(current_user)
		}
		response["id"] = message.id.to_s
		render :json => response
	end

	# def create
	# 	render :json => current_user.sent_messages.create(message_params)
	# end

	# def show
	# 	#@message = current_user.messages.find_message(params[:id])
	# 	@message= Message.find(params[:id])
	# end

	# def reply
	# 	if(params[:message])
	# 		@message = Message.find(params[:id])
	# 		unless @message.receiver.friends.include? current_user
	# 			current_user.accept_user(@message.sender)
	# 			# @message.sender.accept_user(current_user)
	# 		end
	# 		unless @message.nil?
	# 			reply = @message.replies.create(:creator_id=>current_user.id,:reply_message=>params[:message][:reply_message], :created_at => Time.now)
	# 			render :json =>{"id"=>reply.id.to_s,"creator"=>reply.creator,"reply_message"=>reply.reply_message,"created_at"=>reply.created_at}, :status=>201
	# 			return
	# 		end
	# 	end
	# 	render :json =>{:result=>false}, :status=>404
	# end

	def delete_all
		unless params[:ids].nil?
			params[:ids].split(",").each do |id|
				case params[:type]
				when "sent_table"
					message = current_user.sent_messages.find(id)
					message.update_attribute("sender_status", "deleted")
				when "inbox_table"
					message = current_user.received_messages.find(id)
					message.update_attribute("receiver_status","deleted")
				when "deleted_table"
					message = Message.find(id)
					if(message.sender == current_user)
						message.update_attribute("sender_status","deleted_permenant")
					elsif message.receiver == current_user
						message.update_attribute("receiver_status","deleted_permenant")
					end
				end
			end
		end
		render :json =>{:result=>true} ,status: 201
	end

	def contactUs
		contactParams=params["message"].require("data").permit(:username,:email,:subject,:message)
		UserMailer.contact_us_mail(contactParams).deliver
		render :json =>{:result=>true}
	end

	def restore
		reload = nil
		unless params[:ids].nil?
			params[:ids].split(",").each do |id|
				message = Message.find(id)
				if(message.sender == current_user)
					message.update_attribute("sender_status","sent")
					reload = "sent_table"
				elsif message.receiver == current_user
					reload = "inbox_table"
					message.update_attribute("receiver_status","read")
				end
			end
		end
		render :json =>{:reload=>reload} ,status: 201
	end
	
	private
	def message_params
		params.require(:message).permit!
	end
end