json.messages @messages.each do |message|
	json.(message,:message,:subject,:sender_status, :receiver_status)
	json.id(message.id.to_s)
	json.date(message.created_at)
	json.sender(message.sender, :id,:email)
	json.receiver(message.receiver, :id,:email)
end
json.total_entries @messages.total_entries
