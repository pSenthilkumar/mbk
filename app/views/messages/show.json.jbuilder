json.(@message,:message,:subject,:sender_status,:receiver_status)
json.id(@message.id.to_s)
json.date(@message.created_at)
json.sender(@message.sender, :id,:email)
json.receiver(@message.receiver, :id,:email)
if(@message.replies.count>0)
json.replies @message.replies.order_by([:date,:desc]) do |reply|
		json.(reply,:creator,:reply_message,:created_at)
		json.id(reply.id.to_s)
	end
else
	json.replies([])
end