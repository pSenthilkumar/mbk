json.array!(@promocodes) do |promocode|
  json.extract! promocode, :id, :validity, :active, :code
  json.url promocode_url(promocode, format: :json)
end
