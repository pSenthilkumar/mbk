user = @result[:user]

if user.profile.is_public==false && current_user.id!=user.id
	show_user="private"
elsif @result[:isFriend]==false && current_user.profile.paymentStatus!='active' && current_user.id!=user.id
	show_user = "half"
else
	show_user = "full"
end
if(show_user=="private")
	json.user do 
		json.id(user.id.to_s) 
		json.profile do 
			profile = user.profile
			json.name(profile.name)
			json.is_public(profile.is_public)
		end
		json.profileImage(user["profileImage"].file) unless (user["profileImage"].nil?)
		json.last_seen(user.last_seen)
	end
else
	json.user do
		json.id(user.id.to_s)
		json.profile do 
			profile = user.profile
			json.name(profile.name)
			json.tag_line(profile.tag_line)
			json.name(profile.name)
			json.gender(profile.gender)
			json.zodiac(profile.zodiac)
			json.eye_color(profile.eye_color)
			json.hair_color(profile.hair_color)
			json.birth_day(profile.birth_day)
			json.birth_month(profile.birth_month)
			json.age(profile.age)
			json.location(profile.location)
			json.height(profile.height)
			json.body_type(profile.body_type)
			json.religion(profile.religion)
			json.ethnicity(profile.ethnicity)
			json.is_public(profile.is_public)

			if(show_user=="full")
				json.education(profile.education)
				json.drinker(profile.drinker)
				json.go_with_drinker(profile.go_with_drinker)
				json.drug_adict(profile.drug_adict)
				json.go_with_drug_addict(profile.go_with_drug_addict)
				json.go_with_has_pets(profile.go_with_has_pets)
				json.go_with_have_children(profile.go_with_have_children)
				json.go_with_not_believe_karma(profile.go_with_not_believe_karma)
				json.go_with_smoker(profile.go_with_smoker)
				json.have_car(profile.have_car)
				json.have_children(profile.have_children)
				json.have_pets(profile.have_pets)
				json.need_children(profile.need_children)
				json.smoker(profile.smoker)
				json.looking_for(profile.looking_for)
				json.max_age(profile.max_age)
				json.min_age(profile.min_age)
				json.relationship_type(profile.relationship_type)
				json.paymentStatus(profile.paymentStatus)
				json.other_qualities(profile.other_qualities)
				json.income_type(profile.income_type)
				json.long_relationship(profile.long_relationship)
				json.profession(profile.profession)
				json.relationship_status(profile.relationship_status)
				json.bad_karma_points(profile.bad_karma_points)
				json.get_inspired(profile.get_inspired)
				json.good_karma_points(profile.good_karma_points)
				json.my_type_if(profile.my_type_if)
				json.others_interested_about(profile.others_interested_about)
				json.zen_moment(profile.zen_moment)
			end
		end
		json.profileImage(user["profileImage"].file) unless (user["profileImage"].nil?)
		json.last_seen(user.last_seen)
	end
end
json.images(user.images) 	if(show_user=="full")
json.isFriend(@result[:isFriend])
json.paymentStatus(current_user.profile.paymentStatus=='active')