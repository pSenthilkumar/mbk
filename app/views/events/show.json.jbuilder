json.(@event,:event_name,:address,:city,:state,:zip_code,:event_date,:time_from,:time_to)
json.id(@event.id.to_s)
json.date(@event.created_at)
json.creator(@event.user, :id,:email)
participants = @event.participants.map { |p| p.email}
json.participants(participants)
json.my_event(@event.user==current_user)
json.participated(@event.participants.include?(current_user))

