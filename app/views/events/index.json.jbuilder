json.events @events.each do |event|
	json.(event,:event_name,:address,:city,:event_date,:time_from,:time_to)
	json.id(event.id.to_s)
	json.user(event.user, :id,:email)
	unless event.participants.count==0
		participants = event.participants.map { |p| p.email}
		json.participants(participants)
	else
		json.participants("-")
	end
end
json.total_pages @total_pages