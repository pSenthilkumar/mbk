myApp.controller('userController',
	['$scope','$location','Auth','$http','$timeout','$rootScope','$routeParams','$http','Userfactory','Profile','Images','Search','$mdSidenav',
		function($scope,$location,Auth,$http,$timeout,$rootScope,$routeParams,$http,Userfactory,Profile,Images,Search,$mdSidenav,blockUI) {
			
			$scope.viewedUsers=[];
			$scope.userModel = Userfactory.model
			$rootScope.currentUserDetail = Userfactory.model.detail
			$scope.promoCodeValue = ""
			$scope.updated={}
			// $scope.registerForm.password.$error = Userfactory.model.regError
			
			$scope.contactUs=function() {
				console.log($scope.contactData);
				Userfactory.contactUs($scope.contactData).then(function(response) {
					$scope.contactMessageSent=Userfactory.model.contactMessageSent
				})
			}

			$scope.login = function(){
				Userfactory.login($scope.user)
			}

			$scope.register = function(){
				Userfactory.register($scope.user);
			}

			$scope.saveCustomer = function(status, response) {
				$scope.cardError = {}
				if(response.error === undefined){
					$scope.paymentProgress = "Proccessing"
					$http.post('/users/subscribe', { token: response.id }).success(function(){
						$scope.paymentProgress = "Paid"
						Userfactory.model.detail.profile.paymentStatus = 'active'
						$scope.subscripe = false
					});
				} else {
					$scope.paymentProgress = "Subscribe"
					$scope.cardError = response.error
				}
			};
			$scope.updatePassword = function(){
				$http.post('/users/updatePassword', { data:$scope.changePassword })
						 .success(function(data){
								if(data == true) {
									location.reload();
								}
								$scope.changePasswordError = !data
						});

			}

			if($routeParams.show=="favSection"){
				Search.query({get: "favourites"}).then(function(response){
					$scope.favourites = response.data
				});
			}

			$scope.cancelSubscription = function(){
				if (confirm("Are you sure?")) {
					$http.post('/users/unsubscribe').success(function(data){
						Userfactory.model.detail.profile.paymentStatus = ""
						$scope.subscripe = false
					});
				}
			}

			$scope.forgotPassword = function(){
				$scope.forgotPasswordError = ''
				$scope.forgotPasswordMailSending = true;
				Userfactory.forgotPassword($scope.forgotPasswordEmail).then(
					function(data){
						if(data.status == 201){
							$scope.forgotPasswordMailSending= false
							$scope.forgotPasswordMailSent = true
						}
					},
					function(response){
						$scope.forgotPasswordMailSending = false;
						$scope.forgotPasswordError = response.data.errors
					}
				);
			}
			if($routeParams.show == 'picturesSection'){
			if($scope.files === undefined){
				Images.query().then(function(data){
					$scope.files = data;
				})
			} else {
				$scope.files = []
			}
		}
			// $scope.files = []

			$scope.uploadFile = function(files){
				var totalFiles = $scope.files.length + files.length;
				if(totalFiles < 19){
					angular.forEach(files, function(value, key) {
						$scope.files.push(value);
					});
					startIndex = $scope.files.length - files.length;
					for (var i = 0; i < files.length; i++) {
						var fd = new FormData();
						$scope.files[startIndex+i].status = "Uploading"
						fd.append("file", files[i]);
						fd.append("id",startIndex+i);
						$http.post('/image', fd, {
							withCredentials: true,
							headers: {'Content-Type': undefined },
							transformRequest: angular.identity
						}).success( function(data){
							if(data.status != false) {
								$scope.files[data.id].status = "Success";
								$scope.files[data.id] = data.file
							} else {
								$scope.files[data.id].status = "Error";
							}
						}).error(function(){
							console.log("Failure");
						});
					}
				}
			}

			$scope.setPublic = true
			$scope.changeView = function(){
				$scope.setPublic = !$scope.setPublic
				$scope.show_private = !$scope.show_private
			}

			$scope.removeImage = function(file,index) {
				if (confirm("Are you sure?")) {
					Images.$get("/image/"+file["_id"]["$oid"],{todo:'delete'}).then(function(){
						// $("#"+file["_id"]["$oid"]).remove();
						$scope.files.splice(index, 1);
					})
				}
			}

			$scope.setProfilePic = function(file,index) {
				for (var i = 0; i < $scope.files.length; i++) {
					$scope.files[i].profile_pic = false;
				}

				fileUpdate = new Images;
				fileUpdate.id = file["_id"]["$oid"];
				fileUpdate.profile_pic = true
				fileUpdate.update().then(function(data){
					file.profile_pic = data.profile_pic;
				})
			}
			$scope.gender = ['Male','Female']
			$scope.zodiac = ['Aries','Taurus','Gemini','Cancer','Leo','Virgo','Libra','Scorpio','Sagittarius','Capricorn','Aquarius','Pisces']
			$scope.birth_month = ['January','February','March','April','May','June','July','August','September','October','November','December']
			$scope.birth_day = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
			$scope.Height = ['< 5 (< 152 cm)','5.0 (152 cm)','5.1 (155 cm)','5.2 (157 cm)','5.3 (160 cm)','5.4 (163 cm)','5.5 (165 cm)','5.6 (168 cm)','5.7 (170 cm)','5.8 (173 cm)','5.9 (175 cm)','5.10 (178 cm)','5.11 (180 cm)','6.0 (183 cm)','6.1 (185 cm)','6.2 (188 cm)','6.3 (191 cm)','6.4 (193 cm)','6.5 (196 cm)','6.6 (198 cm)','6.7 (201 cm)','6.8 (203 cm)','6.9 (206 cm)','6.10 (208 cm)','6.11 (211 cm)','7.0 (213 cm)','>7 (> 213 cm)']
			$scope.profession = ['Arts and Humanities','Beauty','Psychology','Science and Technology','Health and Medicine/Nursing','Education and Teaching','Computers and IT']
			$scope.bodyType = ['Prefer Not To Say','Thin','Athletic','Average','A Few Extra Pounds','Big & Tall/BBW']
			$scope.ethnicity = ['Caucasian','Black','Hispanic','Indian','Middle Eastern','Asian','Native American','Mixed Race','Other Ethnicity']
			$scope.religion = ['New age','Non-religious','Muslim','Jewish','Catholic','Buddhist','Hindu','Anglican','Sikh','Methodist','Christian - other','Baptist','Lutheran','Presbyterian','Other']
			$scope.political = ['Conservative','Liberal','Middle of the road','Neither']
			$scope.relationShip = ['Hang Out','Friends','Dating','Long Term']
			$scope.relationshipstatus = ['Prefer Not to Say','Married','Living Together','Single','Seperated','Divorced','Widowed','Just Looking','Not Single','Not Looking']
			$scope.longestreslationship =['Prefer Not to Say','Under 1 year','Over 1 year','Over 2 years','Over 3 years','Over 4 years','Over 5 years','Over 6 years','Over 7 years','Over 8 years','Over 9 years','Over 10 years']
			$scope.haircolor = ['Prefer Not to Say','Black','Blond(e)','Brown','Grey','Bald','Mixed color']
			$scope.eyecolor = ['Prefer Not to Say','Blue','Hazel','Grey','Green','Brown','Black','Other']
			$scope.income = ['Prefer Not to Say', 'Less Than 25,000','25,001 to 35,000','35,001 to 50,000','50,001 to 75,000','75,001 to 100,000','100,001 to 150,000','150,000+']
			// $scope.income = ['Select','$','€','₹','£','Lek','؋','ƒ','ман','p.','BZ$','$b','KM','P','лв','R$','៛','¥','₡','kn','₱','Kč','kr','RD$','¢','Q','L','Ft','Rp','﷼','₪','J$','₭','Ls','Lt','ден','RM','₨','₮','MT','C$','₦','₩','B/.','Gs','S/.','zł','lei','руб','Дин.','S','R','CHF','NT$','฿','TT$','₤','₴','$U','Bs','₫','Z$']
			
			// $scope.incometype = ['Prefer Not to Say','$20000','$30000','$40000','$50000','$60000','$70000','$80000','$90000','$100000','$110000','$120000','$130000','$140000','$150000']
			$scope.education_list = ['High school','Some college','Some University','Associates degree','Bachelors degree','Masters degree','PhD / Post Doctoral','Graduate degree']
			
			$scope.loadInterest = function(value){
				values = ['Cricket','Games','Coding','Travel','Movie','Acting','Networking','Yoga']
				var results = [];
				var match = new RegExp('^'+value,"i");
				for (var i = 0; i < values.length; i++) {
					if (match.test(values[i])) {
						results.push(values[i]);
					}
				}
				return results
			}

			$scope.updateProfile = function(form){
				console.log(form)
				if(form.$valid){
					$scope.profile.save().then(function(data){
						Userfactory.model.detail.profile = data.data
						$scope.profile = data.data
						$scope.updated[form.$name] = true
						$timeout(function(){
							$scope.updated[form.$name]= false
						},4000)
						Userfactory.model.profile_filled = data.data.is_profile_filled
						$rootScope.showNotFilled=!Userfactory.model.profile_filled
					});
				}
			}

			if($routeParams.profile_id!=undefined){
					Profile.get($routeParams.profile_id).then(function(data){
						$scope.user_data = data.originalData.user
						$scope.user_profile = $scope.user_data.profile
						$scope.user_images = data.originalData.images
						$scope.paymentStatus = data.originalData.paymentStatus
						$scope.isFriend = data.originalData.isFriend
						$scope.user_data.isFriend = $scope.isFriend
						if($scope.user_profile.is_public==false)
							$scope.show_private = true
						else if($scope.isFriend==false && $scope.paymentStatus==false && $scope.current_user_id!=$scope.user_data.id)
							$scope.show_full_profile = false 
						else 
							$scope.show_full_profile=true

						angular.forEach(Object.keys($scope.user_profile),function(key){
							if(key!="is_public")
							{ 
								if(eval("$scope.user_profile."+key)==null){
									eval("$scope.user_profile."+key+"= '-';");
								}
								if(eval("$scope.user_profile."+key)==true){
									eval("$scope.user_profile."+key+"= 'Yes';");
								}  
								if(eval("$scope.user_profile."+key)==false){
									eval("$scope.user_profile."+key+"= 'No';");
								}
							}                          
						})
					})
			}
			$scope.report_abuse = function(){
				if($routeParams.profile_id!=undefined){
					Profile.reportAbuse($routeParams.profile_id)
				}
			}

			$scope.subscribePromo = function(){

				req = {
				 method: 'POST',
				 url: '/subscribePromo',
				 data: { code: $scope.promoCodeValue }
			 }
			 $http(req).success(function(data){
						if(data==true){
						Userfactory.model.detail.profile.paymentStatus = "active"
						$scope.subscripe = true
					}
				});

			}

			$scope.makePublic = function(public_status){
				$scope.profile.is_public=public_status
				$scope.profile.save()
			}

			$scope.show = 'aboutSection'

			if($routeParams.show !== undefined) {
				$scope.show = $routeParams.show
			}
			if($location.path()=="/myprofile/aboutSection" || $location.path()=="/myprofile")
			{
				if($scope.profile==undefined && $rootScope.loggedUser==true)  {
					Profile.get().then(function(data){
						if(data.data){
							$scope.profile = data.data
							Userfactory.model.detail.profile = $scope.profile
						}
					})
				
					Profile.viewed_me().then(function(data){
						$scope.viewed_me = data
					})
				}
			}
			$scope.openProfileNav= function(){
				$mdSidenav('profile-nav-tab').toggle()
			}
		}
	]
)