myApp.controller('messageController', ['$scope','$filter','$timeout','$location','$resource','$q','Auth','$routeParams','$rootScope','$http','Userfactory','Message','ngTableParams','$mdSidenav','Faye',
	function($scope,$filter,$timeout,$location,$resource,$q,Auth,$routeParams,$rootScope,$http,Userfactory,Message,ngTableParams,$mdSidenav,Faye,$mdToast,$animate) {

	  $scope.userModel = Userfactory.model
    Userfactory.users_for_select()
	  Userfactory.invitations()
    $rootScope.currentUserDetail = Userfactory.model.detail

    $scope.count = {};
  
    $scope.acceptFriend = function(user){
      Userfactory.acceptFriend(user)
    }



    $scope.sendMessage = function(){
      if($scope.message !== '' && $scope.message !== undefined)
      Faye.publish($scope.currentChatUser.user_id,$scope.currentThreadId, {
        msg: $scope.message,
        id: $scope.userModel.detail["_id"]["$oid"]
      }).then(function(response){
        $scope.messages[$scope.currentChatUser.user_id].push(response.data.message);
      })
      
      $scope.message = ""

      $(".chat-message-list").animate({
        scrollTop: $('.chat-message-list')[0].scrollHeight
      }, 1000);
    }

    $scope.messages = {}
    
    $scope.setCurrentUser = function(user){
      $scope.currentChatUser = user
      $scope.count[user.user_id] = undefined
      Message.query({user:user}).then(function(response){
        $scope.messages[user.user_id] = response.data.replies.messages
        $scope.currentThreadId = response.data.id
      })
    }

    if($routeParams.receipient_id === undefined){
      if($scope.userModel.friends.length > 0){
        $scope.setCurrentUser($scope.userModel.friends[0])
      }
    } else {
      user = {
        "user_id": $routeParams.receipient_id
      }
      $scope.setCurrentUser(user)
    }

		$scope.toggleLeft = function() {
	    $mdSidenav('left').toggle()
	  }
	  $scope.toggleRight = function() {
	    $mdSidenav('right').toggle()
	  }
	  $scope.leftClose = function() {
    	$mdSidenav('left').close()
  	}
  	$scope.rightClose = function() {
    	$mdSidenav('right').close()
  	}
	}
])
