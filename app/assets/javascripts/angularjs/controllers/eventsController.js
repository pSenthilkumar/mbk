myApp.controller('eventController', ['$scope','$filter','$timeout','$location','$resource','$q','Auth','$rootScope','$routeParams','$http','Userfactory','Event','ngTableParams','$mdDialog','$mdSidenav', function($scope,$filter,$timeout,$location,$resource,$q,Auth,$rootScope,$routeParams,$http,Userfactory,Event,ngTableParams,$mdDialog,$mdSidenav) {
	$scope.event_obj ={}
	$scope.show_individual=false
	$scope.my_events = new ngTableParams({
	    page: 1,            // show first page
	    count: 10,          // count per page
	    total: 0},
	    {         // length of data
	    	getData: function($defer, params) {
	        // ajax request to api
	        query_params = {"page":params.page(),"search":$scope.my_events_search,"type":"my_events"}
	        Event.query(query_params).then(function(data) {
          // update table params
          params.total(data.originalData.total_entries);
          // set new data
          $defer.resolve(data.originalData.events);
      });
	    }
	});

	$scope.all_events = new ngTableParams({
		page: 1,            // show first page
		count: 10,          // count per page
		total: 0},
		{         // length of data
			getData: function($defer, params) {
		// ajax request to api
		query_params = {"page":params.page(),"search":$scope.all_events_search,"type":"all_events"}
		Event.query(query_params).then(function(data) {
			// update table params
			params.total(data.originalData.total_entries);
			// set new data
			$defer.resolve(data.originalData.events);
		});
	}
});

	$scope.participated_events = new ngTableParams({
		page: 1,            // show first page
		count: 10,          // count per page
		total: 0},
		{         // length of data
			getData: function($defer, params) {
		// ajax request to api
		query_params = {"page":params.page(),"search":$scope.participated_events_search,"type":"participated"}
		Event.query(query_params).then(function(data) {
			// update table params
			params.total(data.originalData.total_entries);
			// set new data
			$defer.resolve(data.originalData.events);
		});
	}
});

	$scope.deleted_events = new ngTableParams({
		page: 1,            // show first page
		count: 10,          // count per page
		total: 0},
		{         // length of data
			getData: function($defer, params) {
		// ajax request to api
		query_params = {"page":params.page(),"search":$scope.deleted_events_search,"type":"deleted"}
		Event.query(query_params).then(function(data) {
			// update table params
			params.total(data.originalData.total_entries);
			// set new data
			$defer.resolve(data.originalData.events);
		});
	}
});

	$scope.open_participants = function($event) {
		var parentEl = angular.element(document.body);
		$mdDialog.show({
			parent: parentEl,
			targetEvent: $event,
			template:
			'<md-dialog class="Participants" aria-label="List dialog">' +
			'  <md-content><p class="Participants-List-Title">Participants list</p>'+
			'	 <hr></hr>'+
			'    <md-list>'+
			'      <md-item ng-repeat="user in participants">'+
			'       <p><a><i class="fa fa-user"></i> {{user}}</a></p>' +
			'      </md-item>'+
			'    </md-list>'+
			'  </md-content>' +
			'  <div class="md-actions">' +
			'    <md-button class="btn waves-effect waves-light Close-Button" ng-click="closeDialog()">' +
			'      Close' +
			'    </md-button>' +
			'  </div>' +
			'</md-dialog>',
			locals: {
				participants: $scope.show_event.participants
			},
			controller:function(scope,$mdDialog,participants){
				scope.participants = participants
				scope.closeDialog = function() {
					$mdDialog.hide();
				}
			}
		});
	}

	$scope.$watch('all_events_search', function () {
		$scope.all_events.reload();
	});
	$scope.$watch('my_events_search', function () {
		$scope.my_events.reload();
	});
	$scope.$watch('deleted_events_search', function () {
		$scope.deleted_events.reload();
	});
	$scope.$watch('participated_events_search', function () {
		$scope.participated_events.reload();
	});
	$scope.createEvent = function(){	
		new Event({"event":$scope.event_obj}).save();
		$scope.event_obj ={}
		$scope.set_tab(3)
	}

	$scope.select_all = function(select_all_type){
		data = eval("$scope."+select_all_type+".data");
		angular.forEach(data,function(evt){
			evt.$selected=true
		})
	}
	
	$scope.show_event_obj = function(event_id){
		Event.get(event_id).then(function(data){
			$scope.show_event = data.data
			$scope.show_individual = true
		})
	}


	$scope.unparticipate = function(){
		table_data= $scope.participated_events.data
		filteredData = $filter('filter')(table_data,{$selected:true})
		ids = []
		angular.forEach(filteredData,function(evt){
			ids.push(evt.id);
		})
		params = {"ids":ids.join(",")}
		Event.unparticipate_events(params).then(function(data){
			$scope.participated_events.reload()
		})

	}

	$scope.delete_event = function(){
		$scope.show_event.delete().then(function(data){
			$scope.deleted_events.reload()
			$scope.show_event ={}
			$scope.back_to_table();
		});
	}
	
	$scope.participate_event = function(event_id){
		Event.$get("events/participate/"+event_id).then(function(data){
			if(data.originalData.result==true){
				$scope.show_event.participated = true 
			}
		})
	}

	$scope.back_to_table = function(){
		$scope.show_individual=false;
	}
	$scope.deleteEvents = function(table_type){

		table_data = eval("$scope."+table_type+".data")
		filteredData = $filter('filter')(table_data,{$selected:true})
		ids = []
		angular.forEach(filteredData,function(evt){
			ids.push(evt.id);
		})
		params = {"ids":ids.join(",")}
		if(table_type=="deleted_events"){
			params.delete_type="permenant"
		}
		Event.$get("events/delete_all",params).then(function(data){
			$scope.deleted_events.reload();
		})
	}
	$scope.tab_id = 1;

	$scope.set_tab = function(tab_id){
		jQuery('.button-collapse').sideNav('hide');

		$scope.tab_id = tab_id
		$scope.show_individual = false
		tab_ids = ["dummy","all_events","participated_events","my_events","new_event","deleted_events"]
		if(tab_id!=4)
			eval("$scope."+tab_ids[tab_id]+".reload()");
	}
	$scope.is_tabs_selected = function(tab_id){
		return $scope.tab_id ==tab_id
	}
	$scope.userModel = Userfactory.model
      
  $rootScope.currentUserDetail = Userfactory.model.detail
  $scope.paymentStatus= $scope.userModel.detail.profile.paymentStatus
  $scope.toggleEventNav = function(){
		$mdSidenav('event-nav-tab').toggle()
  }
}])
