myApp.controller('searchController',
  ['$scope','$location','Auth','$rootScope','$routeParams','$http','Userfactory','Search','Profile','SavedSearch','$mdSidenav',
    function($scope,$location,Auth,$rootScope,$routeParams,$http,Userfactory,Search,Profile,SavedSearch,$mdSidenav) {
      $scope.show_search_result=true
      $scope.advanced_search_page = 1
      $scope.savesearch = {}
      $scope.searchResults = []
      $scope.userModel = Userfactory.model
      
      $rootScope.currentUserDetail = Userfactory.model.detail

      $scope.basicsearch={}
	    $scope.gender = ['Male','Female']

      Search.query({limit: 16, get: "recent"}).then(function(response){
        $scope.recentUsers = response.data
      });

      $scope.show = 'basicSearch'
      if($routeParams.type !== undefined) {
        $scope.show = $routeParams.type
        if($scope.show == 'advancedSearch')
          $scope.show_search_result = false
      }

      $scope.makeFavourite = function(user){
        user.favourite = !user.favourite
        Profile.$get('/makeFavourite', {id : user["_id"]["$oid"]}) 
      }

      $scope.basicSearch = function(){
        Search.query({get: "results",filters: $scope.basicsearch}).then(function(response){
          $scope.searchResults = response.data
          $scope.closeFilter()
        });
      }

      $scope.requestFriend = function(userId){
        Userfactory.requestFriend(userId).then(function(data){
        })
      }

      $scope.deleteSearch = function(index){
        $scope.saved_searches[index].id = $scope.saved_searches[index]["_id"]["$oid"]
        $scope.saved_searches[index].remove().then(function(data){
          $scope.saved_searches.splice(index,1)
        });
      }

      $scope.saveSearch = function(){
        $scope.savesearch.filters = $scope.advancesearch
        new SavedSearch($scope.savesearch).save().then(function(data){
          $scope.saved_searches.push(data)
          $scope.open_save_search_form =false          
        })
      }
      if($rootScope.loggedUser == true){
      SavedSearch.query().then(function(data){
        $scope.saved_searches = data
      })
      }
      $scope.loadSearch = function(search){
        $scope.advancesearch = search.filters
        $scope.advanced_search_page = 1
        $scope.open_save_search_form = false
      }

      $scope.advancedSearch = function(next_page){
        if(next_page==true){
          $scope.advanced_search_page++
        }
        if($scope.advancesearch!=undefined || true){
          limit=3;
          Search.query({get: "results",filters: $scope.advancesearch,page:$scope.advanced_search_page,limit:limit})
            .then(function(data){
              result = data.originalData;
              $scope.searchResults = $scope.searchResults.concat(result.users)
              $scope.show_search_result=true
              $scope.show=""
              $scope.starting_row = (result.current_page-1)*limit+1
              $scope.ending_row = $scope.starting_row+result.users.length-1
              $scope.total_records = result.total_entries
              $scope.open_back = true
              if(data.originalData.total_pages>1 && data.originalData.total_pages>$scope.advanced_search_page){
                $scope.load_more = true
              } else {
                $scope.load_more = false
              }
          });
        }
      }
      $scope.usernamesearch = function(){
        Search.query({get: "results",filters: $scope.usersearch}).then(function(response){
          $scope.searchResults = response.data
        });
      }

      $scope.interestsearch = function(){
        Search.query({get: "results",filters: $scope.userinterest}).then(function(response){
          $scope.searchResults = response.data
        });
      }
      $scope.religion = ['New age','Non-religious','Muslim','Jewish','Catholic','Buddhist','Hindu','Anglican','Sikh','Methodist','Christian - other','Baptist','Lutheran','Presbyterian','Other']
      $scope.zodiac = ['Aries','Taurus','Gemini','Cancer','Leo','Virgo','Libra','Scorpio','Sagittarius','Capricorn','Aquarius','Pisces']
      $scope.gender = ['Male','Female']
      $scope.bodyType = ['Prefer Not To Say','Thin','Athletic','Average','A Few Extra Pounds','Big & Tall/BBW']
      $scope.ethnicity = ['Middle Eastern','Native American','Mixed Race','Other Ethnicity']     
      $scope.political = ['Conservative','Liberal','Middle of the road','Neither']
      $scope.relationShip = ['Hang Out','Friends','Dating','Long Term']
      $scope.relationshipstatus = ['Prefer Not to Say','Married','Single','Seperated','Divorced','Widowed','Just Looking','Not Single','Not Looking']
      $scope.longestreslationship =['Prefer Not to Say','Under 1 year','Over 1 year','Over 2 years','Over 3 years','Over 4 years5','Over 5 years','Over 6 years','Over 7 years','Over 8 years','Over 9 years','Over 10 years']
      $scope.haircolor = ['Prefer Not to Say','Black','Blond(e)','Brown','Grey','Bald','Mixed color']
      $scope.eyecolor = ['Prefer Not to Say','Blue','Hazel','Grey','Green','Brown','Black','Other']
      $scope.income = ['Select','$','€','₹','£','Lek','؋','ƒ','ман','p.','BZ$','$b','KM','P','лв','R$','៛','¥','₡','kn','₱','Kč','kr','RD$','¢','Q','L','Ft','Rp','﷼','₪','J$','₭','Ls','Lt','ден','RM','₨','₮','MT','C$','₦','₩','B/.','Gs','S/.','zł','lei','руб','Дин.','S','R','CHF','NT$','฿','TT$','₤','₴','$U','Bs','₫','Z$']
      $scope.income = ['Prefer Not to Say', 'Less Than 25,000','25,001 to 35,000','35,001 to 50,000','50,001 to 75,000','75,001 to 100,000','100,001 to 150,000','150,000+']
      $scope.education_list = ['High school','Some college','Some University','Associates degree','Bachelors degree','Masters degree','PhD / Post Doctoral','Graduate degree']

      $scope.loadInterest = function(){
        return ['Cricket','Games','Coding','Travel','Movie','Acting','Networking','Yoga'];
      }
      $scope.openSearchNav = function(){
        $mdSidenav('search-nav-tab').open();
      } 
      $scope.openFilter = function(){
        $mdSidenav('basic-search-filter').open();
      }
      $scope.closeFilter = function(){
        $mdSidenav('basic-search-filter').close();
      }
      $scope.backToAdvancedSearch = function(){
        $scope.show = 'advancedSearch'
        $scope.show_search_result = false
      }
    }
  ]
)