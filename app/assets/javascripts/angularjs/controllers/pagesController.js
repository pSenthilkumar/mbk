myApp.controller("pagesController",['$scope','$rootScope','$routeParams','$sce','StaticPage',function($scope,$routeScope,$routeParams,$sce,StaticPage){

	if($routeParams.pageName!=undefined)
	{
		StaticPage.get($routeParams.pageName).then(function(data){
		  $scope.page_content = $sce.trustAsHtml(data.page_content)
		  $scope.page_title = data.page_title
		})
	}
}])
