myApp.directive("searchContainer", function() {
	return {
		restrict: "C",
		link: function(scope, element, attrs) {
			$('select').material_select();

			$(".button-collapse").sideNav();

			var s= $('.message-menu');
			$('.button-collapse').click(function(){
				s.animate({left:'0'});
			});
		}
	};
});

myApp.directive("manualUpload", function() {
	return {
		restrict : "AC",
		link: function (scope, elem) {
			elem[0].addEventListener("change", function (changeEvent) {
				scope.$apply(function () {
					scope.uploadFile(changeEvent.target.files);
				});
			});
		}
	}
});

myApp.directive('fancybox', function() {
	var linker = function(scope,element,attr){
		target= angular.element("[rel='" + attr.rel + "']")
		target.fancybox();
	};
	return{
		restrict : "C",
		link: linker
	}
});

myApp.directive('magnificpopup',function() {
	return {
		restrict: 'C',
		link: function($scope, element, attr) {
			element.magnificPopup({
				delegate: 'a', 
				type: 'image',
				gallery: {
					enabled: true,          
				}
			});
		}
	}
});


myApp.directive('sideNav',function() {
	return {
		restrict: 'A',
		link: function($scope, element, attr) {
				$('[id="sidenav-overlay"]').remove();
				jQuery(element).sideNav({edge: attr.sideNav,closeOnClick: true})
			}
		};
});
myApp.directive('loadingContainer', function () {
	return {
		restrict: 'A',
		scope: false,
		link: function(scope, element, attrs) {
			var loadingLayer = angular.element('<div class="loading"></div>');
			element.append(loadingLayer);
			element.addClass('loading-container');
			scope.$watch(attrs.loadingContainer, function(value) {
				loadingLayer.toggleClass('ng-hide', !value);
			});
		}
	};
});

myApp.directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if(event.which === 13) {
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter, {'event': event});
				});

				event.preventDefault();
			}
		});
	};
});