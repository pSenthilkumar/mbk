myApp.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $routeProvider
    .when("/",
      { 
        templateUrl: "/assets/index.html",
        controller: "userController",
        login: false
      })

    .when("/login",
      { 
        templateUrl: "/assets/user/login.html",
        controller: "userController",
        login: false
      })
    .when("/home",
      { 
        templateUrl: "/assets/user/home.html",
        controller: "userController",
        login: true
      })
    .when("/viewUsers",
      { 
        templateUrl: "/assets/user/viewUsers.html",
        controller: "userController",
        login: true
      })
    .when("/myprofile",
      { 
        templateUrl: "/assets/user/profile.html",
        controller: "userController",
        login: true
      })
    .when("/profile/:profile_id",
      { 
        templateUrl: "/assets/user/view_profile.html",
        controller: "userController",
        login: true
      })
    .when("/myprofile/:show",
      { 
        templateUrl: "/assets/user/profile.html",
        controller: "userController",
        login: true
      })
    .when("/favorites",
      { 
        templateUrl: "/assets/user/favorites.html",
        controller: "searchController",
        login: true
      })
    .when("/viwedme",
      { 
        templateUrl: "/assets/user/viewed.html",
        controller: "userController",
        login: true
      })
    .when("/register",
      { 
        templateUrl: "/assets/user/register.html",
        controller: "userController",
        login: false
      })
    .when("/forgotPassword",
      { 
        templateUrl: "/assets/user/forgotPassword.html",
        controller: "userController",
        login: false
      })
    .when("/dashboard",
      { 
        templateUrl: "/assets/user/dashboard.html",
        controller: "searchController",
        login: true
      })
    .when("/message",
      { 
        templateUrl: "/assets/features/message.html",
        controller: "messageController",
        login: true
      })
    .when("/message/:receipient_id",
      {
        templateUrl: "/assets/features/message.html",
        controller: "messageController",
        login: true
      })
    .when("/message/:message",
      { 
        templateUrl: "/assets/features/message.html",
        controller: "messageController",
        login: true
      })
    .when("/event",
      { 
        templateUrl: "/assets/features/event.html",
        controller: "eventController",
        login: true
      })    
    .when("/event/:event",
      { 
        templateUrl: "/assets/features/event_detail.html",
        controller: "eventController",
        login: true
      }) 
    .when("/search",
      { 
        templateUrl: "/assets/user/search.html",
        controller: "searchController",
        login: true
      })
    .when("/search/:type",
      { 
        templateUrl: "/assets/user/search.html",
        controller: "searchController",
        login: true
      })    
    .when("/chat",
      { 
        templateUrl: "/assets/features/chat.html",
        controller: "chatController",
        login: true
      })
    .when("/contact_us",
      { 
        templateUrl: "/assets/user/contact_us.html",
        controller: "userController"        
      })
    .when("/page/:pageName",
      { 
        templateUrl: "/assets/static_pages/page.html",
        controller: "pagesController",
        login: undefined
      })
    .otherwise({ redirectTo: "/" });

}]).run( 

 ["$rootScope", "$location", "$route", "$anchorScroll","Auth", "Userfactory", "$timeout", function($rootScope, $location, $route,$anchorScroll,Auth,Userfactory,$timeout) {
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      $('.button-collapse').sideNav('hide');
            $('[id="sidenav-overlay"]').remove();

      if($rootScope.loggedUser === undefined) {
        Userfactory.isLogged().then(function(){
          if($rootScope.loggedUser == true) {
            authenticate(next);
          }
          if($rootScope.loggedUser == false) {
            notLogged(next)
          }
        });
      } else if($rootScope.loggedUser == true) {
        authenticate(next);
      } else if ( $rootScope.loggedUser == false) {
        notLogged(next)
      }
    });
$rootScope.$on('$viewContentLoaded', function(){

var interval = setInterval(function(){
  if (document.readyState == "complete") {
      window.scrollTo(0, 0);
      clearInterval(interval);
  }
});

});

    function notLogged(next){
      $rootScope.loggedUser = false;
      if ( next.login )  {
        $location.path( "/login" );
      }
    }

    function authenticate(next){  
      $rootScope.loggedUser = true;
      if ( !next.login && next.login !== undefined )  {

        $location.path( "/dashboard" );
      }
      if(Userfactory.model.profile_filled!=true && next.$$route.originalPath!="/myprofile"){
        alert('Profile is not filled. Please complete your profile before continue.')
        $location.path( "/myprofile" );
      }
    }
 }]);