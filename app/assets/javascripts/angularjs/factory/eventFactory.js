myApp.factory('EventFactory', ["Auth", "$http", "$location", "$rootScope", "$q", "$timeout","Event", function(Auth,$http,$location,$rootScope,$q,$timeout,Event){
  var EventFactory = {}
  EventFactory.model = { 
  	details:undefined,
  	my_events:{},
  	all_events:{},
  	deleted_events:{},
  	participated_events:{}
  }

  EventFactory.all_events = function(query_params){
    query_params.events_type="all"
  	Event.query(query_params).then(function(response){
  		EventFactory.model.all_events=response.data
  	})
  }
  EventFactory.my_events = function(query_params){
    query_params.events_type="my_events"
  	Event.query(query_params).then(function(data){
  		EventFactory.model.my_events=data
  	})
  }
  EventFactory.deleted_events = function(query_params){
    query_params.events_type="deleted"
  	Event.query(query_params).then(function(data){
  		EventFactory.model.deleted_events=data
  	})
  }

  EventFactory.participated_events = function(query_params){
    query_params.events_type="participated"
  	Event.query(query_params).then(function(data){
  		EventFactory.model.participated_events=data
  	})
  }

  EventFactory.create = function(event_obj){
  	new Event(event_obj).create().then(function(data){
  		EventFactory.model.details = data;
  	})
  }

  EventFactory.participate = function(event_obj){
  	Event.$get("/events/"+ event_obj.id+"participate/").then(function(data){
  	})
  }
  return EventFactory;

}]);


