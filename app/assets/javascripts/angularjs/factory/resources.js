myApp.config(["railsSerializerProvider", function(railsSerializerProvider) {
    railsSerializerProvider.underscore(angular.identity).camelize(angular.identity);
}]);

myApp.factory('Event', ['railsResourceFactory','railsSerializer' ,function (railsResourceFactory,railsSerializer) {
	var Event =  railsResourceFactory({
	  url: '/events',
	  name: 'event',
    fullResponse: true
	});
  Event.unparticipate_events = function(params){
    return this.$get("events/unparticipate",params)
  }
	return Event;
}]);
myApp.factory('Message', ['railsResourceFactory','railsSerializer' ,function (railsResourceFactory,railsSerializer) {
  var Message =  railsResourceFactory({
    url: '/messages',
    name: 'message',
    fullResponse: true
  });
  Message.contactUs = function(data){
    return this.$post("/messages/contactUs",{"data":data})
  }
  return Message;
}]);

myApp.factory('Reply', ['railsResourceFactory','railsSerializer' ,function (railsResourceFactory,railsSerializer) {
  var Reply =  railsResourceFactory({
    url: '/reply',
    name: 'reply',
    fullResponse: true
  });
  return Reply;
}]);


myApp.factory('Profile', ['railsResourceFactory','railsSerializer', function (railsResourceFactory,railsSerializer) {
  var profile = railsResourceFactory({
    url: '/profile',
    name: 'profile',
    fullResponse:true
  });
  profile.viewed_me = function(){
    return this.$get("/profile/viewed_me")
  }
  profile.reportAbuse = function(id){
    return this.$get("/profile/report_abuse",{"id":id})
  }
  
  return profile;
}]);

myApp.factory('User', ['railsResourceFactory','railsSerializer', function (railsResourceFactory,railsSerializer) {
  var profile = railsResourceFactory({
    url: '/profile',
    name: 'profile',
  });
  return profile;

}]);
myApp.factory('Images', ['railsResourceFactory','railsSerializer', function (railsResourceFactory,railsSerializer) {
  var image = railsResourceFactory({
    url: '/image',
    name: 'image',
  });
  return image;
}]);

myApp.factory('Search', ['railsResourceFactory','railsSerializer', function (railsResourceFactory,railsSerializer) {
  var search = railsResourceFactory({
    url: '/search',
    name: 'search',
    fullResponse: true
  });
  return search;
}]);

myApp.factory('SavedSearch', ['railsResourceFactory','railsSerializer', function (railsResourceFactory,railsSerializer) {
  var search = railsResourceFactory({
    url: '/saved_searches',
    name: 'saved_search',
  });
  return search;
}]);

myApp.factory('StaticPage', ['railsResourceFactory', function (railsResourceFactory) {
  var staticPage = railsResourceFactory({
    url: '/static_pages',
    name: 'static_page',
  });
  return staticPage;
}]);
// angular.module('User').factory('Book', ['railsResourceFactory', function (railsResourceFactory) {
//     return railsResourceFactory({url: '/users', name: 'user'});
// }]);