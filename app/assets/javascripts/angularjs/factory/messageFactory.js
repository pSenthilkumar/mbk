myApp.factory('MessageFactory', ["Auth", "$http", "$resource","$location", "$rootScope", "$q", "$timeout","Message", function(Auth,$http,$resource,$location,$rootScope,$q,$timeout,Message){
  var MessageFactory = {}
  MessageFactory.model = { 
  	message:undefined,
  	inbox:[],
  	sent_items:[],
  	deleted:[],
  }

  MessageFactory.getInbox = function(query_params){
    if(query_params==undefined)
      query_params = {}
    query_params.type ="inbox"
  	return Message.query(query_params)
  }
  MessageFactory.getSentItems = function(){
  	return Message.query({type:"sent"}).$promise
  }
  MessageFactory.delete_messages = function(query){
    return Message.delete_messages(query).$promise
  }
  MessageFactory.getDeleted = function(){
  	Message.query({type:"deleted"}).$promise.then(function(data){
  		MessageFactory.sent_items=data
  	})
  }
  return MessageFactory;
}])