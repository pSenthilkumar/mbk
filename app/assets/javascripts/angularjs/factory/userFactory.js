myApp.factory('Userfactory', ["Auth", "$http", "$location", "$rootScope", "$q", "$timeout",'$mdToast','$animate', 'Message', function(Auth,$http,$location,$rootScope,$q,$timeout,$mdToast,$animate,Message){
  var Userfactory = {}
  
  Userfactory.model = {
    detail: undefined,
    active: false,
    loginError: undefined,
    regError: {},
    friends: [],
    invitations: [],
    profile_filled:undefined,
    contactMessageSent:false
  }

  Userfactory.toastPosition = {
    bottom: false,
    top: true,
    left: false,
    right: true
  };
  Userfactory.getToastPosition = function() {
    return Object.keys(Userfactory.toastPosition)
      .filter(function(pos) { return Userfactory.toastPosition[pos]; })
      .join(' ');
  };

  Userfactory.contactUs=function(data) {
    return Message.contactUs(data).then(function(response) {
      Userfactory.model.contactMessageSent=response.data.result
    })
  }

  Userfactory.subscribeToFaye = function(){
    PrivatePub.subscribe("/"+Userfactory.model.detail["_id"]["$oid"], function(response, channel){
      
      if(response.data.type == 'reply'){
        if(angular.element('.chat-message-list').scope() === undefined){
          content = response.data.message[3]+" : "+response.data.message[1]
          $mdToast.show(
            $mdToast.simple()
              .content(content)
              .position(Userfactory.getToastPosition())
              .hideDelay(3000)
          );
        } else {
          angular.element('.chat-message-list').scope().messages[response.data.message[2]["$oid"]].push(response.data.message);
          $(".chat-message-list").animate({
            scrollTop: $('.chat-message-list')[0].scrollHeight
          }, 1000);
        }
      }

      if(response.data.type == "notification"){
        content = response.data.message
        $mdToast.show(
          $mdToast.simple()
            .content(content)
            .position(Userfactory.getToastPosition())
            .hideDelay(3000)
        );
      }
    })
  }
  
  Userfactory.isLogged = function(){

    if(!Userfactory.model.active){
      return Auth.currentUser().then(function(user) {
        Userfactory.model.detail = user
        Userfactory.model.profile_filled = user.profile.is_profile_filled
        $rootScope.loggedUser = true
        $rootScope.current_user_id = user["_id"]["$oid"]

        $rootScope.showNotFilled=!Userfactory.model.profile_filled
        Userfactory.users_for_select();
        Userfactory.invitations();
        Userfactory.subscribeToFaye()
      }, function(error) {
        $rootScope.loggedUser = false;
      });
    } else {
      return Userfactory.model.active
    }
  }

  Userfactory.login = function(user) {
    Auth.login(user).then(function(user) {
      Userfactory.model.detail = user
      $rootScope.current_user_id = user["_id"]["$oid"]
      Userfactory.model.profile_filled = user.profile.is_profile_filled
      $rootScope.loggedUser = true;
      Userfactory.users_for_select();
      $rootScope.showNotFilled=!Userfactory.model.profile_filled
      Userfactory.model.loginError = ""
      Userfactory.invitations();
      Userfactory.subscribeToFaye()
      $location.path( "/dashboard" );
    }, function(error) {
      Userfactory.model.loginError = error.data.error
    });
  }

  Userfactory.forgotPassword = function(emailToSend){
    var user = {user:{email:emailToSend}};
    return $http.post('/users/password.json', user);
  }

  Userfactory.users_for_select = function(){
    return $http.get('/users/users_for_select.json').then(function(response){
      Userfactory.model.friends = response.data
    });;
  }

  Userfactory.requestFriend = function(userId){
    return $http.post('/users/requestFriend',{id:userId}).then(function(response){
    });;
  }

  Userfactory.acceptFriend = function(user){
    return $http.post('/users/acceptFriend',{id:user.user_id}).then(function(response){
      var index = Userfactory.model.invitations.indexOf(user)
      Userfactory.model.invitations.splice(index, 1);
      Userfactory.model.friends.push(user)
    });;
  }

  Userfactory.invitations = function(userId){
    return $http.get('/users/invitations',{id:userId}).then(function(response){
      Userfactory.model.invitations = response.data
    });;
  }

  Userfactory.register = function(user){
    if(user !== undefined) {
      if(user.password == user.password_confirmation) {
        Auth.register(user).then(function(user) {
          Userfactory.model.detail = user
          Userfactory.users_for_select();
          Userfactory.invitations();
          $rootScope.loggedUser = true;
          $location.path("/myprofile")
          Userfactory.subscribeToFaye()
        }, function(error) {
          Userfactory.model.regError = error.data.errors
        });
      } else {
        Userfactory.model.regError = {"conpassword": ["Not matching."]}
      }
    }
  }

  return Userfactory;
}])
