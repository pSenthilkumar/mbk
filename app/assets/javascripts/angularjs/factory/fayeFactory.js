myApp.factory('Faye', ["Auth", "$http", "$resource","$location", "$rootScope", "$q", "$timeout","Reply", function(Auth,$http,$resource,$location,$rootScope,$q,$timeout,Reply){
  var Faye = {}
  Faye.publish = function(chat_user_id,message_id,params){
    var deferred = $q.defer();
    var reply = new Reply()
    reply.reply_message = params.msg
    reply.message_id = message_id
    reply.chat_user_id = chat_user_id
    var response = reply.save()
    if(response){
      deferred.resolve(response)
    }
    return deferred.promise
    // return Message.$post("/sendMessage",{channel: channel, message:params})
  }
  return Faye;
}])