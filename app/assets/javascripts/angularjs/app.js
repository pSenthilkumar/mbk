var myApp = angular.module('myApp',["ngRoute","ngResource","Devise","rails","ngMaterial","ngMessages","ngTable","checklist-model","yaru22.angular-timeago","ngTagsInput","stripe","localytics.directives","ngAutocomplete","blockUI"])

myApp.config(function($httpProvider) {
  $httpProvider.defaults.headers.common['X-CSRF-Token'] =
    $('meta[name=csrf-token]').attr('content');
  $httpProvider.defaults.headers.common['X-Requested-With'] = 'AngularXMLHttpRequest'

  Stripe.setPublishableKey('pk_test_JSMu5hIA6FfCNIEhghSF7UTc');
});