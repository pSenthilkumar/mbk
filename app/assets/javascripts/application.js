// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require angular
//= require angular-resource
//= require angular-messages
//= require angular-animate/angular-animate.min
//= require angular-aria/angular-aria.min
//= require angular-material/angular-material.min
//= require angular-block-ui/dist/angular-block-ui.min.js
//= require angular-chosen-localytics/chosen.js
//= require checklist-model
//= require jquery.fancybox
//= require jquery.magnific-popup.min

//= require ng-table
//= require angularjs/rails/resource
//= require angular-route
//= require angularjs/extDirectives/timeAgo
//= require angularjs/extDirectives/ngAutocomplete
//= require angularjs/app
//= require angularjs/factory/userFactory
//= require angularjs/factory/messageFactory
//= require angularjs/shared/directives
//= require private_pub
//= require ckeditor/init
//= require rails_admin/custom/ckeditor_ajax
//= require_tree .
