# this class is used to mail the users 
class UserMailer < ActionMailer::Base
  default from: "support@mustbekarma.net"

  # Sending mail after signup
  # @param [User] user 
  def welcome_email(user)
    @user = user
    mail(:to => user.email, :subject => "Welcome to my awesome site")
  end

  # send mail if the user get promoted by code
  # @param [User] user
  def promoted_mail(user)
    @user = user
    mail(:to => user.email, :subject => "You are a promoted user")
  end

   # Send mail if the user subscribed
  # @param [User] user
  def subscription(user)
    @user = user
    mail(:to => user.email, :subject => "You are subscribed to Mustbekarma")
  end

  def update_password(user)
    @user = user
    mail(:to=>@user.email,:subject=>"Your password has been changed")
  end

  def consolidated_mail(user,options)
    @user = user
    @options = options
    mail(:to=>@user.email,:subject=>"What are all happened today in MustbeKarma?. Please checkout")
  end
  # Sending mail if the user cancelled the subscription
  # @param [User] user
  def subscription_cancel(user)
    @user = user
    mail(:to => user.email, :subject => "Your subscription cancelled.")
  end

  # # Send warning when subscription about to expire
  # # @param [User] user 
  # def warning_promotion(user)
  #   @user = user
  #   mail(:to => user.email, :subject => "Your Promotional Subscrpition Expires soon")
  # end

  # # Send message if subscription exipired
  # # @param [User] user
  # def expired_promotion(user)
  #   @user = user
  #   mail(:to => user.email, :subject => "Your Promotional Subscrpition Expired")
  # end

  # Sending mail if the user cancelled the subscription
  # @param [User] user
  # def subscription_cancel(user)
  #   @user = user
  #   mail(:to => user.email, :subject => "Your subscription cancelled. Please login to subscribe")
  # end

  # If the message has been received send mail too
  # @param [User] sender - Sender of the message
  # @param [User] receiver - receiver of the message 
  # @param [String] type - Message type ["Text", "Smiley"] 
  # def notify_mail(sender,receiver,type)
  #   @sender = sender
  #   @receiver = receiver
  #   @type = type
  #   mail(:to=>receiver.email,:subject=>"You have received a #{type} from #{sender.name}.")
  # end

  # Contact us mailing to support
  # @param [Contact] contact
  #
  def contact_us_mail(contactData)
    @contact = contactData
    mail = "support@mustbekarma.net"
    # mail = "kumarks1122@gmail.com"
    # mail = "prabhu@codingmart.com"
    mail(:to=>mail,:subject=>"#{@contact["username"]} has sent a message")
  end

  # If the user deactivated then send mail to him
  # @param [User] user
  # def deactivate_mail(user)
  #   @user = user
  #   mail(:to=>@user.email,:subject=>"Suspend user account")
  # end

  # If the user activate the account then send mail to him
  # @param [User] user
  # def activate_mail(user)
  #   @user = user
  #   mail(:to=>@user.email,:subject=>"Activation Of Account")
  # end

  # If the user activate the account then send mail to him
  # @param [User] user
end
